extends "res://characters/Character.gd"

func _init():
	TYPE = "PLAYER"

var state = "default"

func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	_shutUPWARNINGS = delta
	match state:
		"default":
			state_default()
		"swing":
			pass

func state_default():
	controls_loop()
	movement_loop()
	spritedir_loop()
	damage_loop()
	if movedir != Vector2(0,0): # if not standing still
		anim_switch("walk")
	else:
		anim_switch("idle")

func state_swing():
	$SwordFront.visible = true
	state = "default"

func controls_loop():
	var LEFT = Input.is_action_pressed("ui_left")
	var RIGHT = Input.is_action_pressed("ui_right")
	var UP = Input.is_action_pressed("ui_up")
	var DOWN = Input.is_action_pressed("ui_down")
	
	movedir.x = -int(LEFT) + int(RIGHT)
	movedir.y = -int(UP) + int(DOWN)

	if Input.is_action_just_pressed("ui_accept"):
		state = "swing"

func is_moving():
	if movedir != Vector2(0, 0):
		return true
	else:
		return false

func damage_loop():
	for area in $hitbox2.get_overlapping_areas():
		var body = area.get_parent()
		if body.TYPE == "ENEMY":
			alive = false
			get_parent().end_game()
			get_node("Camera2D/DeathScreen/control").visible = true

func disable_screen():
	var g = get_node("Camera2D/DeathScreen/control")
	g.visible = false

func _on_hitbox_body_entered(body):
	_shutUPWARNINGS = body

func _on_hitbox2_body_entered(body):
	_shutUPWARNINGS = body
