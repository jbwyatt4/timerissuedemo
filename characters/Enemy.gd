extends "res://characters/Character.gd"

var state = "default"

var target_loc = Vector2()

var gm_ref = null

# Called when the node enters the scene tree for the first time.
func _ready():
	gm_ref = GM
	speed = 1000

func _physics_process(delta):
	
	match state:
		"default":
			state_default(delta)
		"swing":
			pass

func state_default(delta = 1):
	ai_loop()
	movement_loop(delta)
	spritedir_loop()
	
func ai_loop():
	# Find Player
	target_loc = get_parent().player_ref.position
	# player is above, need to go up
	movedir = Vector2.ZERO
	if position.x > target_loc.x:
		movedir += Vector2(-1, 0)
	else:
		movedir += Vector2(1, 0)

	if position.y > target_loc.y:
		movedir += Vector2(0, -1)
	else:
		movedir += Vector2(0, 1)

func _on_hitbox_body_entered(body):
	_shutUPWARNINGS = body

func _2on_hitbox_body_entered(body):
	if body.name == "PLAYER":
		emit_signal('death', target_loc)