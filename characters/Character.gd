extends KinematicBody2D

#export (int) var health
export (int) var speed
#export (int) var weapon_cooldown

var _shutUPWARNINGS = null

var velocity = Vector2()
#var can_use_weapon = true
var alive = true

var movedir = Vector2(0, 0)
var knockdir = Vector2(0, 0)
var spritedir = "down" # Sprite Direction - what way is the player facing by default?
var TYPE = "ENEMY"
var hitstun = 0

func _ready():
	$SwordFront.position = Vector2(0, 5) + Vector2(16, 16)

func set_sword_front():
	var temp = 5
	var offset = Vector2(16, 16)
	match movedir:
		Vector2(-1,0):
			$SwordFront.position = Vector2(-temp, 0) + offset
		Vector2(1,0):
			$SwordFront.position = Vector2(temp, 0) + offset
		Vector2(0,-1):
			$SwordFront.position = Vector2(0, -temp) + offset
		Vector2(0,1):
			$SwordFront.position = Vector2(0, temp) + offset

func damage_loop():
	for area in $hitbox2.get_overlapping_areas():
		#var body = area.get_parent()
		pass

func movement_loop(delta = 1):
	if GM.is_game_over():
		return
	set_sword_front()
	var motion
	if hitstun == 0:
		motion = movedir.normalized() * speed * delta
	else:
		motion = knockdir.normalized() * 125
	
	_shutUPWARNINGS = move_and_slide(motion, Vector2(0,0)) # Godot needs to be told what direction the char is, 0,0 is used for top down games

func control(delta):
	_shutUPWARNINGS = delta
	pass

func spritedir_loop():
	match movedir:
		Vector2(-1,0):
			spritedir = "left"
		Vector2(1,0):
			spritedir = "right"
		Vector2(0,-1):
			spritedir = "up"
		Vector2(0,1):
			spritedir = "down"
	if movedir != Vector2(0,0):
		#anim_switch("walk")
		pass
	else:
		#anim_switch("idle")
		pass
	
	if Input.is_action_just_pressed("ui_select"):
		#use_item(preload("res://items/sword.tscn"))
		pass

func anim_switch(animation):
	var newanim = str(animation, spritedir)
	if $anim.current_animation != newanim:
		#$anim.play(newanim)
		pass

func _physics_process(delta):
	if not alive:
		return
	if TYPE != "PLAYER":
		control(delta)
		_shutUPWARNINGS = move_and_slide(velocity)