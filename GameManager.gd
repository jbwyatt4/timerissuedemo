extends Node

var game_state = {
	game_over = false,
	map_ref = null
}

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func is_game_over() -> bool:
	return game_state["game_over"]

func new_game() -> void:
	game_state["map_ref"].new_game()
