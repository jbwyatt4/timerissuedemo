extends Node2D

export (int) var enemies_to_spawn
export (int) var starting_speed

var _shutUPWARNINGS = null

onready var enemy_scene = preload("res://characters/Enemy.tscn")

var enemy_speed = null

var player_ref
var player_origin # needed for new game

var list_of_enemies = []

var rng = RandomNumberGenerator.new()

var map_start_x
var map_start_y
var map_end_x
var map_end_y

# Called when the node enters the scene tree for the first time.
func _ready():
	# create a static body 2D by code, then set it's dimensions to the outer region of the map
	GM.game_state["map_ref"] = self
	print(GM.game_state["map_ref"])
	player_ref = $Player
	rng.randomize()
	set_camera_limits()
	player_origin = $Player.position
	new_game_start()
	
func set_camera_limits():
	
	var map_limits = $Ground.get_used_rect() # get the amount of map used, warning, only applies to a rect, becareful with different maps
	var map_cellsize = $Ground.cell_size # 128 in this case
	
	map_start_x = map_limits.position.x
	map_start_y = map_limits.position.y
	map_end_x = map_limits.end.x
	map_end_y = map_limits.end.y
	
	$Player/Camera2D.limit_left = map_start_x * map_cellsize.x
	$Player/Camera2D.limit_right = map_end_x * map_cellsize.x
	$Player/Camera2D.limit_top = map_start_y * map_cellsize.y
	$Player/Camera2D.limit_bottom = map_end_y * map_cellsize.y

func spawn_enemies():
	for en in enemies_to_spawn:
		spawn_enemy(get_random_empty_space(), enemy_speed)

func get_random_empty_space():
	var temp_x = rng.randi_range(map_start_x, map_end_x)
	var temp_y = rng.randi_range(map_start_y, map_end_y)
	return Vector2(temp_x, temp_y)

func spawn_enemy(position, speed = 50, health = 1, weapon_cooldown = 5):
	_shutUPWARNINGS = health
	_shutUPWARNINGS = weapon_cooldown
	var e = enemy_scene.instance()
	e.position = position * $Ground.cell_size
	e.speed = speed
	#e.health = health
	#e.weapon_cooldown = weapon_cooldown
	self.add_child(e)
	list_of_enemies.append(e)
	return e

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	pass

func _on_EnemySpawnCountdown_timeout():
	if GM.is_game_over():
		return
	enemy_speed = enemy_speed * 1.5
	spawn_enemies()
	$EnemySpawnCountdown.start()

func end_game():
	GM.game_state["game_over"] = true
	$EnemySpawnCountdown.stop()

func new_game():
	print("AAAAA")
	print(GM.game_state["map_ref"])
	$Player.disable_screen()
	print("BBBBB")
	# delete last game's enemies
	for e in list_of_enemies:
		e.queue_free()

	$Player.position = player_origin
	$Player.alive = true
	GM.game_state["game_over"] = false
	print("CCCCC")
	new_game_start()

func new_game_start():
	enemy_speed = starting_speed
	spawn_enemies()
	$EnemySpawnCountdown.start()
